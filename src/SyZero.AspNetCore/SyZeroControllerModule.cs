﻿using Autofac;
using Microsoft.Extensions.DependencyModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Text;
using SyZero.AspNetCore.Controllers;

namespace SyZero.AspNetCore
{
    public class SyZeroControllerModule : Autofac.Module
    {

        protected override void Load(ContainerBuilder builder)
        {

            var asss = new List<Assembly>();
            var deps = DependencyContext.Default;
            var libs = deps.CompileLibraries.Where(lib => (lib.Type == "package" && lib.Name.StartsWith("SyZero") || lib.Type == "project"));//排除所有的系统程序集、Nuget下载包
            foreach (var lib in libs)
            {
                try
                {
                    //System.Console.WriteLine(lib.Name);
                    var assembly = AssemblyLoadContext.Default.LoadFromAssemblyName(new AssemblyName(lib.Name));
                    asss.Add(assembly);
                }
                catch
                {
                }
            }


            var baseType = typeof(SyZeroController);
            builder.RegisterAssemblyTypes(asss.ToArray())
                .Where(m => baseType.IsAssignableFrom(m) && m != baseType).PropertiesAutowired();



        }
    }
}
