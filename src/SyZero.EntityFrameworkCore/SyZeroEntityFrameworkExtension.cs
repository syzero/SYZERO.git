﻿using Autofac;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Design;
namespace SyZero.EntityFrameworkCore
{
   public static class SyZeroEntityFrameworkExtension
    {
        /// <summary>
        /// 注册EntityFramework
        /// </summary>
        /// <typeparam name="TContext"></typeparam>
        /// <param name="builder"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static ContainerBuilder AddSyZeroEntityFramework<TContext>(this ContainerBuilder builder, IConfiguration configuration)
            where TContext: DbContext
        {
            // 首先注册 options，供 DbContext 服务初始化使用
            builder.Register(c =>
            {
                var optionsBuilder = new DbContextOptionsBuilder<TContext>();
                if (configuration.GetConnectionString("type").ToLower() == "mysql")
                    optionsBuilder.UseMySql(configuration.GetConnectionString("sqlConnection"));
                else
                    optionsBuilder.UseSqlServer(configuration.GetConnectionString("sqlConnection"));
                return optionsBuilder.Options;
            }).InstancePerLifetimeScope();
            // 注册 DbContext
            builder.RegisterType<TContext>()
                .AsSelf()
                .InstancePerLifetimeScope();

            return builder;
        }
    }
}
