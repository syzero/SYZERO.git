﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using SyZero.Domain.Entities;
namespace SyZero.EntityFrameworkCore
{
    public class SyZeroDbContext<TUser,TContext> : DbContext
        where TUser : SyZeroUser
        where TContext : SyZeroDbContext<TUser, TContext>
    {
        public SyZeroDbContext()
        {
        }

        public SyZeroDbContext(DbContextOptions<TContext> options) : base(options)
        {

        }
        public DbSet<TUser> User { get; set; }
      
        public DbSet<Config> Config { get; set; }
     
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // modelBuilder.Entity<ArticleEntity>().HasIndex(u => u.Id).IsUnique();
        }
    }
}
