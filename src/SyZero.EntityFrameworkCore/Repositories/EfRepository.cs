﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Microsoft.EntityFrameworkCore;
using SyZero.Domain.Repository;
using System.Threading;
using System.Threading.Tasks;
using SyZero.Domain.Entities;

namespace SyZero.EntityFrameworkCore.Repositories
{
    public class EfRepository<TDbContext, TEntity> : IRepository<TEntity>
        where TEntity : class, IEntity
        where TDbContext : DbContext
    {
        private  TDbContext _dbContext;
        private DbSet<TEntity> _dbSet;
        public EfRepository(TDbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<TEntity>();
        }

        #region Count
        public long Count(Expression<Func<TEntity, bool>> where)
        {
            return _dbSet.LongCount(where);
        }

        public  Task<long> CountAsync(Expression<Func<TEntity, bool>> where, CancellationToken cancellationToken = default(CancellationToken))
        {
            return  _dbSet.LongCountAsync(where, cancellationToken);
        }
        #endregion

        #region Insert
        public TEntity Add(TEntity entity)
        {
            var result = _dbSet.Add(entity);
            return result.Entity;
        }

        public Task<TEntity> AddAsync(TEntity entity, CancellationToken cancellationToken = default(CancellationToken))
        {
            return Task.Run(() => Add(entity), cancellationToken);
        }

        public int AddList(IQueryable<TEntity> entities)
        {
            _dbSet.AddRange(entities);
            return 0;
        }

        public  Task<int> AddListAsync(IQueryable<TEntity> entities, CancellationToken cancellationToken = default(CancellationToken))
        {
            return Task.Run(() => AddList(entities), cancellationToken);
         
        }
        #endregion

        #region Delete
        public long Delete(long id)
        {
            var entity = GetModel(id);
            if (entity == null) return 0;
            _dbSet.Remove(entity);
            return 0;
        }

        public long Delete(Expression<Func<TEntity, bool>> where)
        {
            var query = GetList(where);
            if (query == null || !query.Any()) return 0;
            _dbSet.RemoveRange(query);
            return 0;
        }

        public  Task<long> DeleteAsync(long id, CancellationToken cancellationToken = default(CancellationToken))
        {
            return Task.Run(() => Delete(id), cancellationToken);
         
        }

        public  Task<long> DeleteAsync(Expression<Func<TEntity, bool>> where, CancellationToken cancellationToken = default(CancellationToken))
        {
            return Task.Run(() => Delete(where), cancellationToken);
        }
        #endregion

        #region Select
        public IQueryable<TEntity> GetList()
        {
            return _dbSet;
        }

        public IQueryable<TEntity> GetList(Expression<Func<TEntity, bool>> where)
        {
            return _dbSet.Where(where);
        }

        public  Task<IQueryable<TEntity>> GetListAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return  Task.Run(() => GetList(), cancellationToken);
        }

        public  Task<IQueryable<TEntity>> GetListAsync(Expression<Func<TEntity, bool>> where, CancellationToken cancellationToken = default(CancellationToken))
        {
            return  Task.Run(() => GetList(where), cancellationToken);
        }

        public TEntity GetModel(long id)
        {
            return _dbSet.Find(id);
           // return _dbSet.FirstOrDefault(p => p.Id == id);
        }

        public Task<TEntity> GetModelAsync(long id, CancellationToken cancellationToken = default(CancellationToken))
        {
            return Task.Run(() => GetModel(id), cancellationToken);
        
        }

        public TEntity GetModel(Expression<Func<TEntity, bool>> where)
        {
            return _dbSet.FirstOrDefault(where);
        }

        public Task<TEntity> GetModelAsync(Expression<Func<TEntity, bool>> where, CancellationToken cancellationToken = default(CancellationToken))
        {
            return Task.Run(() => GetModel(where), cancellationToken);
        }

        public IQueryable<TEntity> GetPaged(int pageIndex, int pageSize, Expression<Func<TEntity, object>> sortBy, bool isDesc = false)
        {
            if (isDesc)
                return _dbSet.OrderByDescending(sortBy).Skip((pageIndex - 1) * pageSize).Take(pageSize);
            return _dbSet.OrderBy(sortBy).Skip((pageIndex - 1) * pageSize).Take(pageSize);
        }

        public  Task<IQueryable<TEntity>> GetPagedAsync(int pageIndex, int pageSize, Expression<Func<TEntity, object>> sortBy, bool isDesc = false, CancellationToken cancellationToken = default(CancellationToken))
        {
            return  Task.Run(() => GetPaged(pageIndex, pageSize, sortBy, isDesc), cancellationToken);
        }

        public IQueryable<TEntity> GetPaged(int pageIndex, int pageSize, Expression<Func<TEntity, object>> sortBy, Expression<Func<TEntity, bool>> where, bool isDesc = false)
        {
            if (isDesc)
                return _dbSet.Where(where).OrderByDescending(sortBy).Skip((pageIndex - 1) * pageSize).Take(pageSize);
            return _dbSet.Where(where).OrderBy(sortBy).Skip((pageIndex - 1) * pageSize).Take(pageSize);
        }

        public  Task<IQueryable<TEntity>> GetPagedAsync(int pageIndex, int pageSize, Expression<Func<TEntity, object>> sortBy, Expression<Func<TEntity, bool>> where, bool isDesc = false, CancellationToken cancellationToken = default(CancellationToken))
        {
            return  Task.Run(() => GetPaged(pageIndex, pageSize, sortBy, where, isDesc), cancellationToken);
        }
        #endregion

        #region Updata
        public long Update(TEntity entity)
        {
            _dbSet.Update(entity);
            return 0;
        }

        public long Update(IQueryable<TEntity> entitys)
        {
            _dbSet.UpdateRange(entitys);
            return 0;
        }

        public Task<long> UpdateAsync(TEntity entity, CancellationToken cancellationToken = default(CancellationToken))
        {
           return  Task.Run(() => Update(entity), cancellationToken);
        }

        public Task<long> UpdateAsync(IQueryable<TEntity> entitys, CancellationToken cancellationToken = default(CancellationToken))
        {
            return Task.Run(() => Update(entitys), cancellationToken);
        }
        #endregion

    }


}

