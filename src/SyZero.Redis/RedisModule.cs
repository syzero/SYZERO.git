﻿using System;
using System.Collections.Generic;
using Autofac;
using System.Text;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Redis;
using SyZero.Cache;
using Microsoft.Extensions.Configuration;

namespace SyZero.Redis
{
    public class RedisModule : Module
    {
        private IConfiguration _configuration;
        public RedisModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(
                c =>
                {
                    var Redis = new RedisCache(new RedisCacheOptions()
                    {
                        Configuration = _configuration.GetSection("Redis:Connection").Value,
                        InstanceName = _configuration.GetSection("Redis:InstanceName").Value
                    });
                    return Redis;
                })
                .As<IDistributedCache>()
                .InstancePerLifetimeScope().PropertiesAutowired();
           builder.RegisterType<Cache>().As<ICache>().InstancePerLifetimeScope().PropertiesAutowired();
        }
    }
}
