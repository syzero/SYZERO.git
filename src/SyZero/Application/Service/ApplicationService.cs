﻿using System;
using System.Collections.Generic;
using System.Text;
using SyZero.Dependency;
using SyZero.ObjectMapper;
using SyZero.Runtime.Session;

namespace SyZero.Application.Service
{
    public  class ApplicationService : SyZeroServiceBase, IApplicationService, ITransientDependency
    {
        public ISySession SySession { get; set; }
    }

}
