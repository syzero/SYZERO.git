﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SyZero.Application.Service.Dto
{
   public interface IPageAndSortQuery: IPageQuery, ISortQuery, ILimitQuery
    {
    }
}
