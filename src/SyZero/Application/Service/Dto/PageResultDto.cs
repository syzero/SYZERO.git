﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SyZero.Application.Service.Dto
{
    public class PageResultDto<T> : ListResultDto<T>, IPageResult<T>, IListResult<T>
    {
        /// <summary>
        /// 数据总数
        /// </summary>
        public long total { get; set; }

        /// <summary>
        /// 返回分页数据
        /// </summary>
        /// <param name="totalCount">数据总数</param>
        /// <param name="items">列表数据</param>
        public PageResultDto(long totalCount, IReadOnlyList<T> items):base(items)
        {
            this.total = totalCount;
        }
    }
}
