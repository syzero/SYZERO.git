﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SyZero.Application.Service.Dto
{
    public interface IListResult<T>
    {
        /// <summary>
        /// 数据
        /// </summary>
        IReadOnlyList<T> list { get; set; }
    }
}
