﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SyZero.Application.Service.Dto
{
    /// <summary>
    /// ISkipQuery实现
    /// </summary>
    public class PageQueryDto : LimitQueryDto, IPageQuery
    {
        public int Skip { get; set; } = 0;
    }
}
