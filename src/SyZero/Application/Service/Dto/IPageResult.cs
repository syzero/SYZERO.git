﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SyZero.Application.Service.Dto
{
    public interface IPageResult<T>: IListResult<T>
    {
        /// <summary>
        /// 总数
        /// </summary>
        long total { get; set; }
    }
}
