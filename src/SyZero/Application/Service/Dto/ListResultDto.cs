﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SyZero.Application.Service.Dto
{
    public class ListResultDto<T> : IListResult<T>
    {
        public IReadOnlyList<T> list { get; set; }

        /// <summary>
        /// 返回列表数据
        /// </summary>
        public ListResultDto()
        {

        }

        /// <summary>
        /// 返回列表数据
        /// </summary>
        /// <param name="items">列表数据</param>
        public ListResultDto(IReadOnlyList<T> items)
        {
            list = items;
        }
    }
}
