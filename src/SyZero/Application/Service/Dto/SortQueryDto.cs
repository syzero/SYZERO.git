﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SyZero.Application.Service.Dto
{
    public class SortQueryDto:ISortQuery
    {
        public string Sort { get; set; }
    }
}
