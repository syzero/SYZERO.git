﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SyZero.Dependency;

namespace SyZero.Cache
{
    public interface ICache : ITransientDependency
    {
        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool Exist(string key);
        /// <summary>
        /// 获取值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="isExisted">是否成功</param>
        /// <returns></returns>
        T Get<T>(string key, out bool isExisted);
    
        /// <summary>
        /// 移除
        /// </summary>
        /// <param name="key"></param>
        void Remove(string key);
        /// <summary>
        /// 移除 (异步)
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Task RemoveAsync(string key);
        /// <summary>
        /// 重置值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="expireTime">过期时间（小时）</param>
        void Refresh(string key);
        /// <summary>
        /// 重置值 (异步)
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Task RefreshAsync(string key);
        /// <summary>
        /// 存储值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value">值</param>
        /// <param name="exprireTime">过期时间（小时）</param>
        void Set(string key, object value, int exprireTime = 24);
        /// <summary>
        /// 存储值 (异步)
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="exprireTime"></param>
        /// <returns></returns>
        Task SetAsync(string key, object value, int exprireTime = 24);
    }
}
