﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SyZero.Runtime.Entities
{
    /// <summary>
    /// MVC 异常拦截 该对象 返回客户端
    /// </summary>
    [Serializable]
    public class SyMessageBox : Exception
    {
        /// <summary>
        /// 异常模型
        /// </summary>
        public SyMessageBoxModel Model { set; get; }

        /// <summary>
        /// 成功消息
        /// </summary>
        public SyMessageBox()
            : base("操作成功!")
        {
            this.Model = new SyMessageBoxModel(this.Message, SyMessageBoxStatus.成功);
        }

        /// <summary>
        /// 失败并返回 消息 | 客户端接收对象 例如: {status=1,data="这是消息!"}
        /// </summary>
        /// <param name="Messager"></param>
        public SyMessageBox(string Message)
            : base(Message)
        {
            this.Model = new SyMessageBoxModel(Message, SyMessageBoxStatus.失败);
        }

        /// <summary>
        /// 自定义异常
        /// </summary>
        /// <param name="Data">客户端接收对象 例如: {status=1,data="这是消息!"}</param>
        public SyMessageBox(object Message)
            : base($"自定义异常 请忽略!{SyMessageBoxStatus.自定义.ToString()}")
        {
            this.Model = new SyMessageBoxModel(Message, SyMessageBoxStatus.自定义);
        }

    }
}
