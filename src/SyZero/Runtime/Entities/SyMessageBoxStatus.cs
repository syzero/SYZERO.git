﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SyZero.Runtime.Entities
{
    /// <summary>
    /// 消息状态
    /// </summary>
    public enum SyMessageBoxStatus
    {
        接口授权码无效 = -3,
        服务端异常 = -2,
        自定义 = -1,
        失败 = 0,
        成功 = 1,
    }
}
