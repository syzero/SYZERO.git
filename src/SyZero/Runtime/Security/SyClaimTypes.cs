﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace SyZero.Runtime.Security
{
   public static class SyClaimTypes
    {
        /// <summary>
        /// UserName.
        /// Default: <see cref="ClaimTypes.Name"/>
        /// </summary>
        public static string UserName { get; set; } = ClaimTypes.Name;

        /// <summary>
        /// UserId.
        /// Default: <see cref="ClaimTypes.NameIdentifier"/>
        /// </summary>
        public static string UserId { get; set; } = ClaimTypes.NameIdentifier;

        /// <summary>
        /// UserRole.
        /// Default: <see cref="ClaimTypes.Role"/>
        /// </summary>
        public static string UserRole { get; set; } = ClaimTypes.Role;


    }
}
